#ifndef PLASTICITY_HELPERS_HPP
#define PLASTICITY_HELPERS_HPP

#include <medusa/Medusa.hpp>

mm::Range<double> matprops_sy{184.8e9, 184.8e9};
mm::Range<double> matprops_ep{0., 10.};

// double sigma_y(double eps_p) {
//     double out_val = 0.;
//     if (eps_p < 0.) {
//         out_val = matprops_sy[0];
//     } else {
//         int min_under = 0;
//         int min_over = matprops_ep.size() - 1;
//         double pos_diff = eps_p - matprops_ep[min_under];
//         double neg_diff = eps_p - matprops_ep[min_over];
//         for (int i = 1; i < static_cast<int>(matprops_ep.size()) - 1; ++i) {
//             double diff = eps_p - matprops_ep[i];
//             if (diff < 0.) {
//                 if (abs(diff) < abs(neg_diff)) {
//                     min_over = i;
//                     neg_diff = diff;
//                 }
//             } else {
//                 if (diff < pos_diff) {
//                     min_under = i;
//                     pos_diff = diff;
//                 }
//             }
//         }
//         out_val = (matprops_sy[min_over] - matprops_sy[min_under]) /
//                       (matprops_ep[min_over] - matprops_ep[min_under]) *
//                       (eps_p - matprops_ep[min_over]) +
//                   matprops_sy[min_over];
//     }

//     return out_val;
// }

// double dsigma_y(double eps_p) {
//     double H = (matprops_sy[1] - matprops_sy[0]) / (matprops_ep[1] - matprops_ep[0]);
//     for (int i = 2; i < matprops_ep.size(); ++i) {
//         if (eps_p > matprops_ep[i - 1]) {
//             H = (matprops_sy[i] - matprops_sy[i - 1]) / (matprops_ep[i] - matprops_ep[i - 1]);
//         }
//     }

//     return H;
// }

double sigma_y(double eps_p) { return 0.45 + 0.2 * eps_p; }
double dsigma_y(double eps_p) { return 0.2; }

template <typename vec_t>
double dxi(vec_t stress, double E, double nu, double dGam, double G) {
    double a1 = (stress(0) + stress(1)) * (stress(0) + stress(1));
    double a2 = (stress(1) - stress(0)) * (stress(1) - stress(0));
    double a3 = stress(2) * stress(2);
    double b1 = 1 + E * dGam / (3. * (1. - nu));
    double b1c = b1 * b1 * b1;
    double b2 = 1. + 2. * G * dGam;
    double b2c = b2 * b2 * b2;

    return -a1 / (9. * b1c) * E / (1. - nu) - 2. * G * (a2 + 4. * a3) / b2c;
}

double epsEq(mm::Vec3d e, double ez) {
    double exey = e[0] - e[1];
    double eyez = e[1] - ez;
    double ezex = ez - e[0];

    return (sqrt(2.) / 3.) * sqrt(exey * exey + eyez * eyez + ezex * ezex + 6. * e[2] * e[2]);
}

#endif /* PLASTICITY_HELPERS_HPP */
