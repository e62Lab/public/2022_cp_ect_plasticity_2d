#include <medusa/Medusa.hpp>
#include <stdlib.h>  // system()

namespace vtk {
    class outputVTK {
    private:
        std::ofstream vtkfile;

    public:
        template<typename domain_t, typename string_t>
        outputVTK(const domain_t d, const string_t name, const string_t fname, const unsigned int time,
                  const std::string outputDirectory = "") {
            std::string outDir = outputDirectory.empty() ? "vtk_out" : outputDirectory;
            std::string execution = "mkdir -p ";

            if (system(execution.append(outDir).c_str())) {
                std::cout << "Couldn't make " + outDir + "/ folder\n";
                return;
            }

            std::stringstream out_fname;

            out_fname << outDir + "/" << fname << "_" << time << ".vtk";

            vtkfile.open(out_fname.str().c_str());

            vtkfile << "# vtk DataFile Version 3.0\n";
            vtkfile << name << " time step " << time << "\n";
            vtkfile << "ASCII\n";
            vtkfile << "DATASET POLYDATA\n";

            vtkfile << "POINTS " << d.size() << " double\n";

            int empty_dim = 3 - d.pos(0).size();

            for (auto i: d.all()) {
                std::stringstream point_data;

                for (auto j: d.pos(i)) {
                    point_data << j << " ";
                }

                for (int j = 0; j < empty_dim; ++j) {
                    point_data << "0 ";
                }

                point_data << "\n";

                vtkfile << point_data.str().c_str();
            }

            vtkfile << "POINT_DATA " << d.size() << "\n";
        }

        template<typename range_t, typename string_t>
        void addScalar(const range_t s, const string_t name) {
            vtkfile << "SCALARS " << name << " double 1\n";
            vtkfile << "LOOKUP_TABLE default\n";

            for (auto i: s) {
                vtkfile << i << " ";
            }

            vtkfile << "\n";
        }

        template<typename range_t, typename string_t>
        void addVector(const range_t s, const string_t name) {
            vtkfile << "VECTORS " << name << " double\n";

            int dim = s[0].size();
            int empty_dim = 3 - dim;

            for (int i = 0; i < s.size() / dim; ++i) {
                std::stringstream point_data;

                for (int j = 0; j < dim; ++j) {
                    point_data << s[i][j] << " ";
                }

                for (int j = 0; j < empty_dim; ++j) {
                    point_data << "0 ";
                }

                point_data << "\n";

                vtkfile << point_data.str().c_str();
            }
        }

        template<typename range_t, typename string_t>
        void addTensor(const range_t s, const string_t name) {
            vtkfile << "TENSORS " << name << " double\n";

            int dim = s[0].size();
            int empty_dim = 9 - dim;

            for (int i = 0; i < s.size() / dim; ++i) {
                std::stringstream point_data;

                for (int j = 0; j < dim; ++j) {
                    point_data << s[i][j] << " ";
                }

                for (int j = 0; j < empty_dim; ++j) {
                    point_data << "0 ";
                }

                point_data << "\n";

                vtkfile << point_data.str().c_str();
            }
        }

        void close() { vtkfile.close(); }
    };
}  // namespace vtk
