//#define EIGEN_DONT_PARALELLIZE
#include <medusa/Medusa.hpp>
#include <Eigen/Core>
#include <Eigen/SparseLU>
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>
#include <stdlib.h>  // system()
// #include <../vtk.hpp>

#include "helpers/plasticity_helpers.hpp"
#include "helpers/math_helper.hpp"
#include "helpers/vtk.hpp"

using namespace mm;
using namespace std;

int main(int argc, char* argv[]) {
    // Initialize timer.
    Timer timer;
    timer.addCheckPoint("start");

    // Read input.
    assert_msg(argc >= 2, "Second argument should be the parameter xml.");
    XML conf(argv[1]);
    const bool debug = conf.get<int>("debug.print") == 1;
    if (debug) {
        cout << "Calculations started ..." << endl;
    }

    // Set num threads.
    omp_set_num_threads(conf.get<int>("sys.num_threads"));

    // Create output.
    stringstream out_hdf;
    out_hdf << conf.get<string>("meta.out_dir") << conf.get<string>("meta.out_file") << ".h5";
    HDF hdf(out_hdf.str(), HDF::DESTROY);
    hdf.writeXML("conf", conf);
    hdf.close();

    // Read parameters.
    const double E = conf.get<double>("material.E");
    const double nu = conf.get<double>("material.nu");
    const double sigma_yield = conf.get<double>("material.yield");
    const double r1 = conf.get<double>("domain.r1");
    const double r2 = conf.get<double>("domain.r2");
    const double sigma_ext = conf.get<double>("material.sigma_ext");
    const double tol = conf.get<double>("model.tolerance");

    // Derived parameters.
    double lam = E * nu / (1 - 2 * nu) / (1 + nu);
    double mu = E / 2 / (1 + nu);
    lam = 2 * mu * lam / (2 * mu + lam);  // plane stress

    if (debug) {
        prn(E);
        prn(nu);
        prn(sigma_yield);
        prn(lam);
        prn(mu);
        prn(sigma_ext);
    }

    Eigen::Matrix<double, 3, 3> D0;
    D0 << (2 * mu + lam), lam, 0., lam, (2 * mu + lam), 0., 0., 0., mu;
    auto C = D0.inverse();

    Eigen::Matrix<double, 3, 3> P;
    P << (2. / 3.), -(1. / 3.), 0., -(1. / 3.), (2. / 3.), 0., 0., 0., 2.;

    // Edge labels.
    const int type_left = conf.get<int>("domain.type_left");
    const int type_right = conf.get<int>("domain.type_right");
    const int type_top = conf.get<int>("domain.type_top");
    const int type_bottom = conf.get<int>("domain.type_bottom");

    // Load steps.
    const int steps = conf.get<int>("model.steps");

    // Discretization.
    const double dx1 = conf.get<double>("domain.dx1");
    const double dx2 = conf.get<double>("domain.dx2");

    // Build domain.
    if (debug) {
        cout << "Building domain ..." << endl;
    }
    timer.addCheckPoint("domain");
    // // Balls.
    // BallShape<Vec2d> ball_r1(0, r1);
    // BallShape<Vec2d> ball_r2(0, r2);
    // BallShape<Vec2d> ball_r22(0, 2 * r2);
    // // Main box.
    // BoxShape<Vec2d> box_r2({0., 0.}, {r2, r2});
    // BoxShape<Vec2d> box_r22({0., 0.}, {2 * r2, 2 * r2});
    // // // Rotated box.
    // // Eigen::Rotation2Dd Q(PI * conf.get<double>("domain.segment_angle") / 180);
    // // auto rotated_box = box_r22.rotate(Q.toRotationMatrix());
    // // Final shape.
    // auto shape = box_r2 - (box_r22 - ball_r2) - ball_r1;  // - rotated_box;
    // auto domain = shape.discretizeBoundaryWithStep(dx);

    // // Assign types.
    // for (int i = 0; i < domain.size(); ++i) {
    //     Vec2d pos = domain.pos(i);
    //     if (abs(pos[0]) < 0.25 * dx) {
    //         domain.type(i) = type_left;
    //     } else if (abs(pos[1]) < 0.25 * dx) {
    //         domain.type(i) = type_bottom;
    //     } else if (abs((pos.norm() - r2)) < 0.25 * dx) {
    //         domain.type(i) = type_outter;
    //     } else {
    //         domain.type(i) = type_inner;
    //     }
    // }
    PolygonShape<Vec2d> poly(
        {{0.0, 0.0}, {3.0, 0.0}, {3.0, 0.75}, {2.0, 0.75}, {1.5, 0.25}, {1.0, 0.75}, {0.0, 0.75}});
    Vec2d notch{1.5, 0.25};
    Vec2d corner{0.0, 0.75};
    auto fn = [=](const Vec2d& p) { return dx1 + dx2* ( (p - notch).norm()/(corner - notch).norm()); };
    auto domain = poly.discretizeBoundaryWithDensity(fn);
    // Assign types.
    for (int i = 0; i < domain.size(); ++i) {
        Vec2d pos = domain.pos(i);
        if (abs(pos[1]) < 0.25 * tol) {
            domain.type(i) = type_bottom;
        } else if (abs(pos[0] - 3.0) < 0.25 * tol) {
            domain.type(i) = type_right;
        } else if (abs(pos[0]) < 0.25 * tol) {
            domain.type(i) = type_left;
        } else {
            domain.type(i) = type_top;
        }
    }
    // Fill
    GeneralFill<Vec2d> fill;
    fill.seed(conf.get<int>("domain.seed"));
    domain.fill(fill, fn);

    for (int i = 0; i < domain.size(); ++i) {
        if ((abs(domain.pos(i, 0) - 1.5) < tol && abs(domain.pos(i, 1) - 0.25) < tol)){
            domain.removeNodes({i});
            i--;
        }
    }

    // Shape difference.
    hdf.atomic().writeDomain("domain", domain);
    int N = domain.size();

    if (debug) {
        cout << "Building domain finished." << endl;
        prn(N);
        cout << "Finding support ..." << endl;
    }

    // Support nodes.
    timer.addCheckPoint("support");
    int support_size = conf.get<int>("approx.support_size");
    if (support_size == -1) {
        support_size =
            2 * binomialCoeff(conf.get<int>("approx.mon_degree") + Vec2d::dim, Vec2d::dim);
    }
    // domain.findSupport(FindClosest(support_size));
    domain.findSupport(FindClosest(support_size).forNodes(domain.interior()));
    domain.findSupport(FindClosest(support_size).forNodes(domain.boundary()).searchAmong(domain.interior()).forceSelf());

    // Engine.
    timer.addCheckPoint("engine");
    const int mon_degree = conf.get<int>("approx.mon_degree");
    const double sigma_w = conf.get<double>("approx.sigma_w");
    using MatrixType = Eigen::Matrix<typename Vec2d::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
    WLS<Monomials<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest>engine(mm::Monomials<Vec2d>(mon_degree), sigma_w); 

    // WLS<Gaussians<Vec2d>, GaussianWeight<Vec2d>, ScaleToFarthest> engine({9, 100.0}, 2.0); // <-- To sploh ne dela.

    // Monomials<Vec<double, 2>> mon(mon_degree);
    // RBFFD<Polyharmonic<double, 5>, Vec<double, 2>, ScaleToClosest> engine({}, mon);
    auto shapes = domain.computeShapes(engine);
    Eigen::SparseMatrix<double, Eigen::RowMajor> M(2 * N, 2 * N);
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(2 * N);

    // Construct implicit operators over our storage.
    auto eop = shapes.explicitOperators();
    auto evop = shapes.explicitVectorOperators();
    VectorField3d stress(N);
    stress.setZero();
    VectorField3d eps(N);
    eps.setZero();
    VectorField3d eps_e(N);
    eps_e.setZero();
    VectorField3d eps_p(N);
    eps_p.setZero();
    ScalarFieldd phi(N);
    phi.setZero();
    ScalarFieldd eps_p_s(N);
    eps_p_s.setZero();
    VectorField2d u(N);
    u.setZero();
    VectorField2d du(N);
    du.setZero();
    VectorField2d df(N);
    ScalarFieldd force(N);
    ScalarFieldd dGam(N);
    dGam.setZero();
    Range<bool> vmsCrit(N);
    Range<Eigen::Matrix<double, 3, 3>> D(N);
    D = D0;
    Range<Eigen::Matrix<double, 3, 3>> A(N);
    double oldForce = 1.;
    rhs.setZero();
    M.setZero();
    M.reserve(shapes.supportSizesVec());
    auto op = shapes.implicitVectorOperators(M, rhs);
    // Set the governing equations and the boundary conditions.
    for (int i : domain.interior()) {
        (lam + mu) * op.graddiv(i) + mu* op.lap(i) = 0.;
    }
    for (int i : domain.types() == type_left) {
        op.eq(0).c(0).value(i) = 0.0;
        op.eq(1).c(1).der1(i, 0) = 0.0;
    }
    for (int i : domain.types() == type_right) {
        // op.eq(1).c(1).value(i) = 0.0;
        // op.eq(0).c(0).der1(i, 1) = 0.0;
        op.traction(i, lam, mu, {1, 0}) = 0.0;
    }
    for (int i : domain.types() == type_top) {
        op.traction(i, lam, mu, domain.normal(i)) = 0.0;
    }
    for (int i : domain.types() == type_bottom) {
        op.eq(1).c(1).value(i) = 0.;
        op.eq(0).c(0).der1(i, 1) = 0.;
    }

    Eigen::BiCGSTAB<decltype(M), Eigen::IncompleteLUT<double>> solver;
    solver.preconditioner().setFillfactor(30);
    solver.preconditioner().setDroptol(1e-5);
    solver.compute(M);
    auto solution = rhs;
    solution.setZero();
    auto solution_cpy0 = solution;
    auto solution_cpy1 = solution;

    // Load loop.
    int initT = 1;
    for (int t = initT; t <= steps; ++t) {
        prn(t);
        double tension = static_cast<double>(t) / static_cast<double>(steps) * sigma_ext;
        prn(tension);
        // (re)set load step dependent variables
        du.setZero();
        df.setZero();
        dGam.setZero();
        vmsCrit = false;
        auto eps_old = eps;
        int it = 0;
        do {
            rhs.setZero();
#pragma omp parallel for
            for (int i : domain.all()) {
                if (domain.type(i) > 0) {
                    rhs(i) = -df(i, 0);  // residual force
                    rhs(i + N) = -df(i, 1);
                } else {
                    if (domain.type(i) == type_left) {
                        rhs(i) = 0.0;
                        rhs(i + N) = 0.0;
                    } else if (domain.type(i) == type_top) {
                        // traction free
                        auto normal = domain.normal(i);
                        double t0 = normal(0) * stress(i, 0) + normal(1) * stress(i, 2);
                        double t1 = normal(0) * stress(i, 2) + normal(1) * stress(i, 1);
                        rhs(i) = 0. - t0;
                        rhs(i + N) = 0. - t1;
                    } else if (domain.type(i) == type_bottom) {
                        rhs(i) = 0.;
                        rhs(i + N) = 0.;
                    } else {
                        // prescribed sxx (tension)
                        auto normal = domain.normal(i);
                        double t0 = stress(i, 0);
                        double t1 = stress(i, 2);
                        rhs(i) = tension - t0;
                        rhs(i + N) = 0. - t1;
                    }
                }
            }
            if (it == 0) {
                solution = solver.solveWithGuess(rhs, solution_cpy0);
                solution_cpy0 = solution;
            } else {
                solution = solver.solveWithGuess(rhs, solution_cpy1);
                solution_cpy1 = solution;
            }
            du = VectorField2d::fromLinear(solution);
            u += du;  // update du
#pragma omp parallel
            {
#pragma omp for
                for (int i = 0; i < N; ++i) {
                    eps(i, 0) = eop.d1(u.col(0), 0, i);
                    eps(i, 1) = eop.d1(u.col(1), 1, i);
                    eps(i, 2) =
                        (eop.d1(u.col(1), 0, i) + eop.d1(u.col(0), 1, i));  // update total eps
                    eps_e(i) +=
                        (eps(i) -
                         eps_old(i));     // new elastic eps is modified by the change in total eps
                    eps_old(i) = eps(i);  // save old data
                    stress(i) = D0 * eps_e(i);  // update stress through elastic eps
                    double sig_y_temp = sigma_y(eps_p_s(i));
                    phi(i) = 0.5 * stress(i).transpose() * P * stress(i) -
                             (1. / 3.) * sig_y_temp * sig_y_temp;  // update VMS yield  criterion
                    vmsCrit[i] = false;
                    if (phi(i) > 0.) vmsCrit[i] = true;
                }
            }  // pragma omp parallel
            force.setZero();
            if (phi.maxCoeff() >
                0.0) {  // execute if this isn't the first global update at this increment
#pragma omp parallel
                {
#pragma omp for
                    for (int i : domain.all()) {
                        if (vmsCrit[i]) {
                            dGam(i) = 0.;
                            Eigen::MatrixXd Ebd = (C + dGam(i) * P).inverse();
                            double xi_temp = stress(i).transpose() * (Ebd * C).transpose() * P *
                                             (Ebd * C) * stress(i);
                            double H = dsigma_y(eps_p_s(i));
                            double lTol = tol * phi(i);
                            // local N-R phi -> 0, not phi < 0!!!
                            int internalIt = 0;
                            while (abs(phi(i)) > lTol) {
                                double xiDash = dxi(stress(i), E, nu, dGam(i), mu);
                                double eps_p_mod = eps_p_s(i) + dGam(i) * sqrt(2. * xi_temp / 3.);
                                double HBar =
                                    2. * sigma_y(eps_p_mod) * H * sqrt(2. / 3.) *
                                    (sqrt(xi_temp) + dGam(i) * xiDash / (2. * sqrt(xi_temp)));
                                double phiDash = 0.5 * xiDash - (1. / 3.) * HBar;

                                dGam(i) -= phi(i) / phiDash;
                                Ebd = (C + dGam(i) * P).inverse();
                                eps_p_mod = eps_p_s(i) + dGam(i) * sqrt(2. * xi_temp / 3.);
                                xi_temp = stress(i).transpose() * (Ebd * C).transpose() * P *
                                          (Ebd * C) * stress(i);
                                phi(i) = 0.5 * xi_temp -
                                         (1. / 3.) * sigma_y(eps_p_mod) * sigma_y(eps_p_mod);
                                H = dsigma_y(eps_p_mod);
                                internalIt++;
                                if (internalIt > 30) {
                                    break;
                                }
                            }

                            // update local vars
                            eps_p_s(i) += dGam(i) * sqrt(2. * xi_temp / 3.);
                            A[i] = Ebd * C;
                            stress(i) = A[i] * stress(i);
                            eps_e(i) = C * stress(i);
                            eps_p(i) += dGam(i) * P * stress(i);
                            eps_old(i) = eps_e(i) + eps_p(i);  // it may have been modified
                        }
                    }
                }  // pragma omp parallel
#pragma omp parallel
                {
#pragma omp for
                    for (int i : domain.interior()) {
                        auto der = evop.grad(stress, i);
                        df(i, 0) = der(0, 0) + der(2, 1);
                        df(i, 1) = der(2, 0) + der(1, 1);  // update internal force
                        force(i) = sqrt(pow(df(i, 0), 2) +
                                        pow(df(i, 1), 2));  // compute local internal force
                    }
                }  // pragma omp parallel
            }
            bool oFChck = false;
            if (debug && !(it % 100)) {
                std::cout << t << " " << it << " " << force.maxCoeff() << " " << phi.mean() << " "
                          << dGam.mean() << "\n";
            }
            it++;
        } while (force.mean() * (dx1 + dx2) * 0.5 > tol);

        // Output.
        ScalarFieldd vmsRatio(N);
        vmsRatio.setZero();
#pragma omp parallel for
        for (int i : domain.all()) {
            vmsRatio(i) = sqrt(1.5 * stress(i).transpose() * P * stress(i));
        }

        // HDF.
        hdf.setGroupName(format("/load_%06d/", t));
        hdf.atomic().writeEigen("u", u);
        hdf.atomic().writeEigen("stress", stress);
        hdf.atomic().writeEigen("eps", eps);
        hdf.atomic().writeEigen("eps_p", eps_p);
        hdf.atomic().writeEigen("eps_e", eps_e);
        hdf.atomic().writeEigen("df", df);
        hdf.atomic().writeEigen("eps_p_s", eps_p_s);
        hdf.atomic().writeEigen("vmsRatio", vmsRatio);
        hdf.atomic().writeDoubleAttribute("tension", tension);

        // vtk output
        std::stringstream fname;
        fname << "vms_2d_block_" << steps << "_" << N;
        vtk::outputVTK ofile(domain, "von Mises stress", fname.str().c_str(), t,
                             "../results/vtk_out");
        ofile.addVector(u, "displacement");
        ofile.addVector(stress, "stress");
        ofile.addVector(eps, "eps");
        ofile.addVector(eps_p, "eps_p");
        ofile.addVector(eps_e, "eps_e");
        ofile.addVector(df, "df");
        ofile.addScalar(eps_p_s, "eps_p_s");
        ofile.addScalar(vmsRatio, "vmsRatio");
        ofile.close();
    }

    // Complete timer.
    timer.addCheckPoint("end");
    if (debug) {
        prn(timer.duration("start", "end"));
    }
    // Save timer.
    hdf.atomic().writeTimer("timer", timer);

    return 0;
}