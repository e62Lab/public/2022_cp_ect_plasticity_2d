# Two-dimensional perfect plasticity in meshless methods

Conference paper
for [The Eleventh International Conference on Engineering Computational Technology 2022](http://www.ectconference.com)
on _von Mises_ plasticity model with meshless methods.

## Abstract

TODO

## Description

Solution to two-dimensional plasticity using von Mises plasticity model with linear hardening (
perfect plasticity).

## Visuals

For progress and certain visuals please check the `root/resultS/`. It might also be beneficial to
check the `root/logs/`

## Installation

Requirements:

- CMake
- C++ compiler
- Python 3.8.10 (or higher)
- Jupyter notebooks

## Usage

Create or go to `build/` directoy and build using

```bash
cmake .. && make -j 12
```

The executable will be created in `bin/` directory. The executable must be run with a parameter with
all the settings, e.g.

```bash
./plasticity ../input/settings.xml
```

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) for support.

## Contributing

Entire [E62 team](https://e6.ijs.si/parlab/).

## Authors and contributors

- **Mitja Jančič** and
- **Filip Strniša** under supervision of
- **Gregor Kosec**

## Acknowledgments

The authors would like to acknowledge the financial support of the ARRS research core funding No.
P2-0095, ARRS project funding No. J2-3048 and the World Federation of Scientists.

# Reproducing paper results

Build and navigate to `bin/` directory, then:

- To reproduce the results from a synthetic example, run
  ```bash
  ./plasticity ../input/settings.xml
  ```

# License

The project is open source and free to use.

# Important

> **Warning** this repo is closed, because I did not find a convergent state. The figure 7.30(b) from de Souza book on page 256 was completely different.
